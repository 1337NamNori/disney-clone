import { auth, provider } from "../firebase";
import { setSignOut, setUserLogin } from "../features/user/userSlice";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

export const useAuth = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const loginHandler = () => {
        auth.signInWithPopup(provider)
            .then(result => {
                const user = result.user;
                dispatchLogin(user);
            })
    }

    const dispatchLogin = (user) => {
        dispatch(setUserLogin({
            name: user.displayName,
            email: user.email,
            photo: user.photoURL,
        }));
        history.push('/');
    }

    const logoutHandler = () => {
        auth.signOut().then(() => {
            dispatch(setSignOut());
            history.push('/login')
        })
    }

    return {
        dispatch,
        loginHandler,
        logoutHandler,
        dispatchLogin,
    }
}
