import React from 'react';
import Header from './components/Header';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import Home from './components/Home';
import Detail from "./components/Detail";
import Login from "./components/Login";

function App() {
    return (
        <div className="App">
        <Router>
            <Header />
            <Switch>
                <Route path="/movies/:id">
                    <Detail />
                </Route>
                <Route path="/login">
                    <Login />
                </Route>
                <Route path="/" exact>
                    <Home />
                </Route>
            </Switch>
        </Router>
    </div>
  );
}

export default App;
