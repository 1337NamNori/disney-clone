import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyCH2-V7jHnrKkoIdHDrmg1-PuwEBFODzro",
    authDomain: "disney-plus-8898a.firebaseapp.com",
    projectId: "disney-plus-8898a",
    storageBucket: "disney-plus-8898a.appspot.com",
    messagingSenderId: "588802365684",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();
const storage = firebase.storage();

export { auth, provider, storage };
export default db;