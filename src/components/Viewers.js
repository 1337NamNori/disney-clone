import React from 'react';
import styled from "styled-components";

const Container = styled.div`
  padding: 50px 0 20px;
  display: grid;
  grid-template-columns: repeat(5, minmax(0, 1fr));
  grid-gap: 25px;
  
  @media screen and (max-width: 768px) {
    grid-template-columns: repeat(1, minmax(0, 1fr));
  }
`

const Wrap = styled.div`
  position: relative;
  border: 3px solid rgba(249, 249, 249, 0.1);
  border-radius: 10px;
  box-shadow: rgb(0 0 0 / 69%) 0 26px 30px -10px,
              rgb(0 0 0 / 73%) 0 16px 10px -10px;
  cursor: pointer;
  overflow: hidden;
  transition: all 250ms cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;

  video {
    object-fit: cover;
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    display: none;
  }
  
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
  
  &:hover {
    transform: scale(1.05);
    border-color: rgba(249, 249, 249, 0.8);
    box-shadow: rgb(0 0 0 / 80%) 0 40px 58px -16px,
                rgb(0 0 0 / 72%) 0 30px 22px -10px;
    
    video {
      display: block;
      pointer-events: none;
    }
  }
`

function Viewers() {
    return (
        <Container>
            <Wrap>
                <video src="/videos/1564674844-disney.mp4" autoPlay loop playsInline muted>
                </video>
                <img src="/images/viewers-disney.png" alt=""/>
            </Wrap>
            <Wrap>
                <video src="/videos/1564676714-pixar.mp4" autoPlay loop playsInline muted>
                </video>
                <img src="/images/viewers-pixar.png" alt=""/>
            </Wrap>
            <Wrap>
                <video src="/videos/1564676115-marvel.mp4" autoPlay loop playsInline muted>
                </video>
                <img src="/images/viewers-marvel.png" alt=""/>
            </Wrap>
            <Wrap>
                <video src="/videos/1608229455-star-wars.mp4" autoPlay loop playsInline muted>
                </video>
                <img src="/images/viewers-starwars.png" alt=""/>
            </Wrap>
            <Wrap>
                <video src="/videos/1564676296-national-geographic.mp4" autoPlay loop playsInline muted>
                </video>
                <img src="/images/viewers-national.png" alt=""/>
            </Wrap>
        </Container>
    );
}

export default Viewers;