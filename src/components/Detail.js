import React, {useEffect, useState} from 'react';
import styled from "styled-components";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faPlay, faPlus, faUsers} from "@fortawesome/free-solid-svg-icons";
import {Redirect, useParams} from "react-router-dom";
import db from "../firebase";

const Container = styled.div`
  position: relative;
  min-height: calc(100vh - var(--header-height));
  padding: 0 calc(3.5vw + 5px);
`

const BackgroundImage = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: -1;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    filter: brightness(0.7);
  }
`

const ImageTitle = styled.div`
  padding: 40px 0;

  img {
    width: 35vw;
    min-width: 270px;
  }
`

const ControlGroup = styled.div`
  padding: 40px 0;
  display: flex;
  align-items: center;

  @media screen and (max-width: 576px) {
    flex-direction: column;
    align-items: start;
  }
`

const PlayBtn = styled.div`
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: var(--white-color);
  color: var(--gray-color);
  font-size: 24px;
  padding: 12px 28px;
  margin: 10px 20px 10px 0;
  cursor: pointer;
  letter-spacing: 2px;
  transition-duration: 250ms;

  span {
    font-size: 14px;
    margin-left: 12px;
  }

  &:hover {
    opacity: 0.8;
  }
`
const TrailerBtn = styled(PlayBtn)`
  background-color: rgba(0, 0, 0, 0.5);
  color: var(--white-color);
  border: 1px solid var(--white-color);
`

const CircleGroup = styled.div`
  display: flex;
  margin: 10px 0;
`

const CircleBtn = styled.div`
  width: 40px;
  height: 40px;
  font-size: 20px;
  border-radius: 100%;
  border: 2px solid var(--white-color);
  margin: 0 6px;
  color: var(--white-color);
  background: rgba(0, 0, 0, 0.6);
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`

const Description = styled.div`
  font-size: 20px;
  line-height: 30px;
`

function Detail() {
    const {id} = useParams();
    const [movie, setMovie] = useState();

    useEffect(() => {
        db.collection("movies")
            .doc(id)
            .get()
            .then((doc) => {
                if (doc.exists) {
                    setMovie({id: doc.id, ...doc.data()});
                } else {
                    return (<Redirect to="/"></Redirect>)
                }
            })
    }, [id]);


    return (
        <Container>
            {movie && (
                <>
                    <BackgroundImage>
                        <img src={movie.backgroundImg} alt=""/>
                    </BackgroundImage>
                    <ImageTitle>
                        <img src={movie.titleImg} alt=""/>
                    </ImageTitle>
                    <ControlGroup>
                        <PlayBtn>
                            <FontAwesomeIcon icon={faPlay} />
                            <span>PLAY</span>
                        </PlayBtn>
                        <TrailerBtn>
                            <FontAwesomeIcon icon={faPlay}/>
                            <span>TRAILER</span>
                        </TrailerBtn>
                        <CircleGroup>
                            <CircleBtn>
                                <FontAwesomeIcon icon={faPlus} />
                            </CircleBtn>
                            <CircleBtn>
                                <FontAwesomeIcon icon={faUsers} />
                            </CircleBtn>
                        </CircleGroup>
                    </ControlGroup>
                        <p>{movie.subTitle}</p>
                    <Description>
                        <p>{movie.description}</p>
                    </Description>
                </>
                )
            }
        </Container>
    );
}

export default Detail;