import React from 'react';
import Slider from 'react-slick'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import styled from "styled-components";

const Carousel = styled(Slider)`
  margin-top: 20px;
  
  ul li button::before {
    font-size: 10px;
    color: var(--white-color);
  }
  
  li.slick-active button::before {
    color: var(--white-color);
  }
  
  .slick-list {
    overflow: visible;
  }
`
const Wrap = styled.div`
  img {
    width: 100%;
    height: 100%;
    min-height: 250px;
    border-radius: 4px;
    object-fit: cover;
    box-shadow: rgb(0 0 0 / 69%) 0 26px 30px -10px, 
                rgb(0 0 0 / 73%) 0 16px 10px -10px;
    border: 4px solid transparent;
    transition-duration: 0.3s;
    cursor: pointer;
    
    &:hover {
      border-color: rgba(249, 249, 249, 0.8);
    }
  }
`

function ImageSlider() {
    const settings = {
        dots: true,
        arrows: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
    }

    return (
        <Carousel {...settings}>
            <Wrap>
                <img src="/images/slider-badging.jpg" alt=""/>
            </Wrap>
            <Wrap>
                <img src="/images/slider-badag.jpg" alt=""/>
            </Wrap>
            <Wrap>
                <img src="/images/slider-scale.jpg" alt=""/>
            </Wrap>
            <Wrap>
                <img src="/images/slider-scales.jpg" alt=""/>
            </Wrap>
        </Carousel>
    );
}

export default ImageSlider;