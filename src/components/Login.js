import React from 'react';
import styled from "styled-components";
import {useAuth} from "../hooks/useAuth";

const Container = styled.div`
  min-height: calc(100vh - var(--header-height));
  padding: 0 calc(3.5vw + 5px);
  background-image: url(/images/login-background.jpg);
  background-size: cover;
  background-position: center;
  display: flex;
  justify-content: center;
`

const Content = styled.div`
  max-width: 800px;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 100px;

  img {
    width: 100%;
    padding: 12px 0;
  }
  
  button {
    width: 100%;
    padding: 12px 0;
    font-size: 24px;
    font-weight: bold;
    color: var(--white-color);
    background-color: rgb(0, 99, 229);
    outline: none;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    transition-duration: 250ms;
    
    &:hover {
      background-color: rgb(19, 119, 229);
    }
  }
  
  p {
    text-align: center;
    font-size: 12px;
    color: rgba(249, 249, 249, 0.8);
    letter-spacing: 1px;
    padding: 12px 0;
  }
`

function Login() {
    const {loginHandler} = useAuth();

    return (
        <Container>
            <Content>
                <img src="/images/cta-logo-one.svg" alt=""/>
                <button onClick={loginHandler}>GET ALL FREE</button>
                <p>Get Premier Access to Raya and the Last Dragon for an additional fee with a Disney+ subscription.
                    As of 03/26/21, the price of Disney+ and The Disney Bundle will increase by $1.</p>
                <img src="/images/cta-logo-two.png" alt=""/>
            </Content>
        </Container>
    );
}

export default Login;