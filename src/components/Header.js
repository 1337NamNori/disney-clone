import React, {useEffect, useState} from 'react'
import styled from 'styled-components'
import {Link } from "react-router-dom";
import { useSelector } from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFilm, faHome, faPlus, faSearch, faStar, faTape} from "@fortawesome/free-solid-svg-icons";
import {
    selectUserName,
    selectUserPhoto,
} from "../features/user/userSlice";
import { auth } from "../firebase";
import { useAuth } from "../hooks/useAuth";

const Nav = styled.nav`
  height: var(--header-height);
  background-color: var(--gray-color);
  padding: 0 36px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const Logo = styled.img`
  width: 80px;
  cursor: pointer;
`

const NavMenu = styled.div`
  display: flex;

  @media screen and (max-width: 900px) {
    display: none;
  }
`

const NavLink = styled.div`
  color: var(--white-color);
  display: flex;
  align-items: center;
  padding: 0 12px;
  cursor: pointer;

  img {
    width: 20px;
    margin-right: 4px;
  }

  span {
    font-size: 13px;
    margin-left: 8px;
    letter-spacing: 1.42px;
    text-transform: uppercase;
    position: relative;

    &:after {
      content: "";
      height: 2px;
      background: var(--white-color);
      position: absolute;
      right: 0;
      bottom: -6px;
      left: 0;
      opacity: 0;
      transform: scaleX(0);
      transform-origin: left center;
      transition: all 0.25s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
    }
  }

  &:hover {
    span:after {
      opacity: 1;
      transform: scaleX(1);
    }
  }
`

const UserImg = styled.div`
  position: relative;
  cursor: pointer;

  img {
    border-radius: 100%;
    object-fit: cover;
    width: 50px;
    height: 50px;
  }

`

const LoginBtn = styled.div`
  cursor: pointer;
  color: var(--white-color);
  font-size: 16px;
  padding: 8px 24px;
  text-transform: uppercase;
  background-color: transparent;
  letter-spacing: 1px;
  border-radius: 4px;
  border: 1px solid var(--white-color);
  transition-duration: 250ms;

  &:hover {
    color: var(--gray-color);
    background-color: var(--white-color);
  }
`

const NavDropdown = styled.div`
  position: absolute;
  bottom: -50px;
  right: 0;
  min-width: 100px;
  border-radius: 10px;
  background-color: var(--gray-color);
  z-index: 99;
  border: 1px solid rgba(249, 249, 249, 0.3);
  overflow: hidden;
`

const NavDropdownItem = styled.div`
  color: var(--white-color);
  padding: 12px 24px;
  transition-duration: 250ms;
  
  &:hover {
    background-color: rgba(249, 249, 249, 0.2);
  }
`

function Header() {
    const {logoutHandler, loginHandler, dispatchLogin} = useAuth();
    const userName = useSelector(selectUserName);
    const userPhoto = useSelector(selectUserPhoto);

    const [showDropdown, setShowDropdown] = useState(false);

    useEffect(() => {
        auth.onAuthStateChanged(user => {
            if (user) dispatchLogin(user);
        })
    }, [dispatchLogin]);



    const toggleDropdown = () => {
        setShowDropdown(!showDropdown);
    }

    return (
        <Nav>
            <Link to="/">
                <Logo src="/images/logo.svg"/>
            </Link>
            <NavMenu>
                <Link to="/">
                    <NavLink href="#">
                        <FontAwesomeIcon icon={faHome}/>
                        <span>HOME</span>
                    </NavLink>
                </Link>
                <NavLink href="#">
                    <FontAwesomeIcon icon={faSearch}/>
                    <span>SEARCH</span>
                </NavLink>
                <NavLink href="#">
                    <FontAwesomeIcon icon={faPlus}/>
                    <span>WATCHLIST</span>
                </NavLink>
                <NavLink href="#">
                    <FontAwesomeIcon icon={faStar}/>
                    <span>ORIGINALS</span>
                </NavLink>
                <NavLink href="#">
                    <FontAwesomeIcon icon={faTape}/>
                    <span>MOVIES</span>
                </NavLink>
                <NavLink href="#">
                    <FontAwesomeIcon icon={faFilm}/>
                    <span>SERIES</span>
                </NavLink>
            </NavMenu>
            {
                userName ? (
                    <UserImg onClick={toggleDropdown}>
                        <img src={userPhoto} alt={userName}/>
                        {showDropdown &&
                        <NavDropdown>
                            <NavDropdownItem onClick={logoutHandler}>Logout</NavDropdownItem>
                        </NavDropdown>
                        }
                    </UserImg>
                ) : (
                    <LoginBtn onClick={loginHandler}>Login</LoginBtn>
                )
            }
        </Nav>
    )
}

export default Header
